"use strict";

// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
// Екранування переводить системний символ в символь для читання за допомогою \.

// Які засоби оголошення функцій ви знаєте?
// function declaration і fuction espression

// Що таке hoisting, як він працює для змінних та функцій?
// hoisting (підняття) дозволяє використовувати  функцію до ії оголошення.

function createNewUser(newUser) {
  const firstName = prompt("Enter your name");
  const lastName = prompt("Enter your last name");
  const birthday = prompt("Enter your birthday", "dd.mm.yyyy");

  this.getLogin = function () {
    const login = firstName[0].toLowerCase() + lastName.toLowerCase();
    return login;
  };
  this.getAge = function () {
    const birthdayDay = birthday.slice(0, 2);
    const birthdayMonth = birthday.slice(3, 5);
    const birthdayYear = birthday.slice(6, 10);
    const currentDateMiliSecond = Date.now();
    const birthdayMiliSecond = Date.parse(
      `${birthdayYear},${birthdayMonth},${birthdayDay}`
    );
    const age =
      (currentDateMiliSecond - birthdayMiliSecond) /
      (365.25 * 24 * 60 * 60 * 1000);
    return Math.trunc(age);
  };
  this.getPassword = function () {
    const password =
      firstName[0].toUpperCase() +
      lastName.toLowerCase() +
      birthday.slice(6, 10);
    return password;
  };
}

const newUser = new createNewUser();
console.log(`Login: ${newUser.getLogin()}`);
console.log(`Age: ${newUser.getAge()}`);
console.log(`Password: ${newUser.getPassword()}`);
